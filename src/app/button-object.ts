export class ButtonObject {
  private _x:number;
  private _y:number;
  private _val:string;


  constructor(x: number, y: number, val: string) {
    this._x = x;
    this._y = y;
    this._val = val;
  }

  set x(value: number) {
    this._x = value;
  }

  set y(value: number) {
    this._y = value;
  }

  set val(value: string) {
    this._val = value;
  }

  get x(): number {
    return this._x;
  }

  get y(): number {
    return this._y;
  }

  get val(): string {
    return this._val;
  }
}
