import {Component, OnInit} from '@angular/core';
import {ButtonObject} from '../button-object';
import {UninformedSearchService} from '../uninformed-search.service';

@Component({
  selector: 'app-click-board',
  templateUrl: './click-board.component.html',
  styleUrls: ['./click-board.component.css']
})
export class ClickBoardComponent implements OnInit {

  constructor(public uninformedSearchService: UninformedSearchService) {
  }

  boardX = 13;
  boardY = 23;
  options = ['.', 'X', 'S', 'O'];
  materialColors = {'.': 'primary', X: 'accent', S: 'error', O: 'warn'};
  board = [];
  selectedCharacter;
  outputText = '';

  ngOnInit(): void {
    this.resetBoard();
    this.selectedCharacter = '.';
  }

  updateOutputText(): void {
    this.outputText = '';
    for (const i of this.board) {
      for (const j of i) {
        this.outputText += j.val;
      }
      this.outputText += '\n';
    }
  }


  clickOnButton(buttonObject): void {
    buttonObject.val = this.selectedCharacter;
    this.updateOutputText();
  }


  loadBoard(inputText): void {

    const lines = inputText.split('\n');
    for (let i = 0; i < this.boardY; i++) {
      for (let j = 0; j < this.boardX; j++) {
        if (i < lines.length && j < lines[i].length) {
          this.board[i][j].val = lines[i][j];
        } else {
          this.board[i][j].val = '.';
        }
      }
    }
    this.updateOutputText();
  }

  selectCharacter(ch): void {
    this.selectedCharacter = ch;
  }


  resetBoard(): void {
    this.board = [];
    for (let i = 0; i < this.boardY; i++) {
      const row = [];
      for (let j = 0; j < this.boardX; j++) {
        if (i === 0 || j === 0 || i === this.boardY - 1 || j === this.boardX - 1) {
          row.push(new ButtonObject(j, i, 'X'));
        } else {
          row.push(new ButtonObject(j, i, '.'));
        }
      }
      this.board.push(row);
    }
    this.updateOutputText();
  }

}
