import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClickBoardComponent } from './click-board.component';

describe('ClickBoardComponent', () => {
  let component: ClickBoardComponent;
  let fixture: ComponentFixture<ClickBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClickBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClickBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
