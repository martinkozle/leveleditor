import { TestBed } from '@angular/core/testing';

import { UninformedSearchService } from './uninformed-search.service';

describe('UninformedSearchService', () => {
  let service: UninformedSearchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UninformedSearchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
