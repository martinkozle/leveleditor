import {Injectable} from '@angular/core';
import {ButtonObject} from './button-object';


@Injectable({
  providedIn: 'root'
})
export class UninformedSearchService {

  constructor() {
  }

  move(matrix, direction, pos): ButtonObject {
    let x = pos.x;
    let y = pos.y;
    while (matrix[y][x].val !== 'X') {
      x += direction[0];
      y += direction[1];
      if (y >= matrix.length || y < 0 || x >= matrix[0].length || x < 0) {
        return pos;
      }
    }
    return matrix[y - direction[1]][x - direction[0]];
  }


  bfs(matrix): string {
    const queue = [];
    let flag = false;
    for (const i of matrix) {
      for (const j of i) {
        if (j.val === 'S') {
          if (flag) {
            return 'Invalid Board!!!';
          }
          flag = true;
          queue.push([j, 'Solution: ']);
        }
      }
    }
    const visited = [];
    while (queue.length > 0) {
      const exam = queue.shift();
      if (!visited.includes(exam[0])) {
        if (exam[0].val === 'O') {
          return exam[1];
        }

        queue.push([this.move(matrix, [0, 1], exam[0]), exam[1] + 'Down ']);
        queue.push([this.move(matrix, [0, -1], exam[0]), exam[1] + 'Up ']);
        queue.push([this.move(matrix, [1, 0], exam[0]), exam[1] + 'Right ']);
        queue.push([this.move(matrix, [-1, 0], exam[0]), exam[1] + 'Left ']);

        visited.push(exam[0]);
      }
    }
    return 'No Solution!!!';
  }


}

